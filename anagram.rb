#!/usr/bin/ruby

class Dictionary
  def initialize(dictpath = "/usr/share/dict/words")
    @dict = open dictpath
    @words = []
    while !@dict.eof do
      fetch_word
    end
    @words.sort_by!(&:length).reverse!.uniq!
  end

  def fetch_word
    word = @dict.readline.chomp.downcase
    return if word.length == 1 && word != 'a' && word != 'i'
    return unless word.match(/[aeiouy]/)
    @words.push word.chars
  end

  def find_anagram(string)
    letters = string.downcase.gsub(/\W/, "").chars
    expand([], letters, @words)
  end

  private def expand(already, letters, words)
    pairs = words.each_with_object([]) do |word, output|
      rem = remnant(letters, word)
      if rem
        if rem.empty?
          puts (already+[word]).map(&:join).join(" ")
        else
          output << [rem, word]
        end
      end
    end.shuffle
    wrds = pairs.transpose.pop
    pairs.each do |lett, word|
      expand(already + [word], lett, wrds)
    end
  end

  private def remnant(ltr, wrd)
    letters = ltr
    word = wrd
    word.each do |c|
      n = letters.index c
      return nil unless n
      letters = letters.take(n)+letters.drop(n+1)
    end
    return letters
  end
end

Dictionary.new.find_anagram(ARGV.join.downcase)


