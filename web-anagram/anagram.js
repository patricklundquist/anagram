function wait(whether) {
  $("#anagrams").css("opacity", whether ? 0.5 : 1.0);
}

function button(word, data, type) {
  let b = $("<button>");
  b.attr("type", "button");
  b.attr("data-word", word);
  b.text(type);
  let keyword = type == "+" ? "with" : "without";
  b.addClass(keyword);
  b.attr("title", `include only acronyms ${keyword} ${word}`);
  b.click(function (event) {
    keyword = $(this).attr("class");
    if (data[keyword].indexOf(word) == -1) {
      data[keyword].push(word);
      if (keyword == "without") {
        var idx = data["with"].indexOf(word);
        if (idx !== -1) {
          data["with"].splice(idx, 1)
        }
      }
    } else {
      if (keyword == "without") {
        var idx = data["without"].indexOf(word);
        if (idx !== -1) {
          data["without"].splice(idx, 1)
        }
      }
    }
    requestAnagrams(data);
  });
  return b;
}

function requestAnagrams(data = {with: [], without: []}) {
  requestData = {
    phrase: data.phrase,
    with: data.with.join(" "),
    without: data.without.join(" ")
  };
  wait(true);
  $.getJSON("./anagram.rb", requestData, function(result) {
    handleAnagramJson(data, result)
  })
  .fail(function() {
    $.getJSON("./anagram_error.json", "", function(result) {
      handleAnagramJson(data, result)
    })
  });
}

function handleAnagramJson(data, result) {
    let anagrams = $("#anagrams");
    anagrams.empty();
    let excludes = $("#excludes");
    excludes.empty();
    let usedWords = [];
    div = $("<div>");
    div.addClass("excludes");
    for (let word of data["without"]) {
      span = $("<span>");
      span.addClass("word");
      span.text(word);
      span.append(button(word, data, "-"));
      div.append(span);
    }
    excludes.append(div);
    for (let anagram of result) {
      div = $("<div>");
      div.addClass("anagram");
      for (let word of anagram) {
        span = $("<span>");
        span.addClass("word");
        span.text(word);
        if (usedWords.indexOf(word) == -1) {
          if (data["with"].indexOf(word) == -1) {
            span.prepend(button(word, data, "+"));
          }
          span.append(button(word, data, "-"));
          usedWords.push(word)
        }
        div.append(span);
      }
      anagrams.append(div);
      wait(false);
    }
  }

function startRequestAnagrams(data = {with: [], without: []}) {
  // Subtract chars in the given param.
  data.subtract = $("#subtract").val().toLowerCase()
      .replace(/\W{2,}/, " ").replace(/[^a-z ]/,"");
  const charCount = {};
  for (const char of data.subtract) {
    charCount[char] = (charCount[char] || 0) + 1;
  }

  result = [];
  for (const char of $("#phrase").val()) {
    if (charCount[char] > 0) {
      charCount[char]--;
    } else {
      result.push(char);
    }
  }
  data.phrase = result.join('');
  requestAnagrams(data);
}

$(function () {
  $("#phrase").focus();
  $("#phrase").keydown(function (event) {
    if (event.which == 13) {
      event.preventDefault(); // h/t Phil Carter https://stackoverflow.com/a/895231/948073
      startRequestAnagrams();
    }
  });
  $("#phrase").keyup(function (event) {
    $("#phrase")
      .val($("#phrase").val().toLowerCase()
      .replace(/\W{2,}/, " ").replace(/[^a-z ]/,""));
  });
  $("#find").click(function () {
    startRequestAnagrams();
  });
  $("#reset").click(function () {
    $("#phrase").val("");
    $("#phrase").focus();
    $("#anagrams").empty();
    $("#excludes").empty();
  });
});
