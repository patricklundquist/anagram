#!/usr/bin/env ruby

require "cgi"
require "json"

$stderr = $stdout

class Dictionary
  def initialize(without, dictpath = "./words")
    @dict = open dictpath, "r:UTF-8"
    @words = []
    while !@dict.eof do
      fetch_word(without)
    end
    @words.sort_by!(&:length).reverse!.uniq!
  end

  def fetch_word(without)
    word = @dict.readline.chomp.downcase
    return if without.include? word
    return if word.length == 1 && word != 'a' && word != 'i'
    return unless word.match(/[aeiouy]/)
    @words.push word.chars
  end

  def find_anagram(string, with = [], limit = 200)
    letters = string.downcase.gsub(/\W/, "").chars
    letters = remnant(letters, with.join.downcase.gsub(/[^a-z]/, "").chars)
    expand(with.map(&:chars), letters, @words, limit, [])
  end

  private def expand(already, letters, words, limit, results)
    return results if results.length >= limit
    return results if words.empty?
    pairs = words.each_with_object([]) do |word, output|
      rem = remnant(letters, word)
      if rem
        if rem.empty?
          result = ((already+[word]).map(&:join)).sort
          results << result unless results.include? result
        else
          output << [rem, word]
        end
      end
    end
    wrds = pairs.transpose.pop
    pairs.each do |lett, word|
      expand(already + [word], lett, wrds, limit, results)
    end
    results # kludge?
  end

  private def remnant(ltr, wrd)
    letters = ltr
    word = wrd
    word.each do |c|
      n = letters.index c
      return nil unless n
      letters = letters.take(n)+letters.drop(n+1)
    end
    return letters
  end
end

puts "Content-Type: application/json\n\n"
params = CGI.new.params
phrase = params['phrase'][0]
with = params['with'].empty? ? [] : params['with'][0].split(/\W/)
without = params['without'].empty? ? [] : params['without'][0].split(/\W/)
puts Dictionary.new(without).find_anagram(phrase, with, 200).to_json


