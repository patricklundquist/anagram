#!/bin/bash

# Copy web files to /var/www/anagram for use with apache2 webserver.
#
# Setup notes:
# - must have ruby installed
# - must have cgi scripts enabled for ruby and /var/www
#   e.g. under directory /var/www/ in /etc/apache2/apache2.conf add:
#      Options +ExecCGI
#      AddHandler cgi-script .rb
#

set -o nounset

# enable cgi
a2enmod cgi

mkdir -p /var/www/html/anagram

# copy files.
cp web-anagram/* /var/www/html/anagram

echo "Deployed."
